'''To_read_csv_file'''
import csv as csv_module
import matplotlib.pyplot as plt


def district_zip_data_fun(csv_file):
    '''district and picode generator'''
    res = {}
    with open(csv_file, 'rt', encoding='utf-8') as file:
        reader = csv_module.DictReader(file)
        for row in reader:
            zipcode = row['Pin_Code']
            district = row['District'].lower()
            res[district] = zipcode
    return res


def find_zip_by_address(words, district_zip_data):
    '''return the district'''
    words = words.lower().split(' ')
    for word in words:
        if word in district_zip_data:
            return word
    return 'Unclassified'


def gather_register_data(csv_file, district_zip_data):
    '''function to get data'''
    res = {}
    with open(csv_file, 'r', encoding='latin-1') as file:
        reader = csv_module.DictReader(file)
        for row in reader:
            year = row['DATE_OF_REGISTRATION']
            year = year[-2:]  # last 2 str for year format is in DD-MM-YY
            if year == '15':
                address = row['Registered_Office_Address']
                dis = find_zip_by_address(address, district_zip_data)
                if dis in res:
                    res[dis] += 1
                else:
                    res[dis] = 1
    return res


def barplot_register_2015(res):
    'barplot'
    xlist = res.keys()
    ylist = res.values()
    bars = plt.bar(xlist, ylist)
    plt.xlabel("District")
    plt.ylabel("Counts")
    plt.title("Company registration in the year 2015 by the district")
    plt.xticks(rotation=90)
    plt.bar_label(bars, labels=ylist)
    plt.show()


def execute():
    '''gather data and plot'''
    csv_file = './Maharashtra.csv'
    distric_csv_file = 'district.csv'
    district_zip_data = district_zip_data_fun(distric_csv_file)
    data = gather_register_data(csv_file, district_zip_data)
    barplot_register_2015(data)
    

if __name__ == '__main__':
    execute()
# End-of-file (EOF)
