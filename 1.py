'''To_read_csv_file'''
import csv as csv_module
import matplotlib.pyplot as plt


def gather_data(csv_file):
    '''function to get data'''
    res = {}
    with open(csv_file, 'r', encoding='latin-1') as file:
        reader = csv_module.DictReader(file)
        for row in reader:
            year = row['DATE_OF_REGISTRATION']
            year = year[-2:]  # taking the last 2 as year format in DD-MM-YY
            if year in res:
                res[year] += 1
            else:
                res[year] = 1
    return res


def barplot(year_register_dict):
    '''barplot'''
    year_list = year_register_dict.keys()
    registerd_count = year_register_dict.values()
    plt.bar(year_list, registerd_count)
    plt.xlabel("Years")
    plt.ylabel("Counts")
    plt.title("Bar Plot of company registration by year")
    tick_positions = list(range(0, 101, 10))
    tick_labels = tick_positions
    plt.xticks(tick_positions, tick_labels)
    plt.show()


def execute():
    '''gather data and plot'''
    csv_file = './Maharashtra.csv'
    year_register_data = gather_data(csv_file)
    barplot(year_register_data)


if __name__ == '__main__':
    execute()
# End-of-file (EOF)
