'''to_read_csv_file'''
import csv as csv_module
import matplotlib.pyplot as plt


def list_index_dict(top5_activity_data):
    '''return a list which contains the index of keys'''
    res = {}
    index = 0
    for business in top5_activity_data:
        res[business] = index
        index += 1
    return res


def last_10_year_dict():
    '''return a list which contains the index of year'''
    res = {}
    index = 0
    for i in range(11, 21):
        res[i] = index
        index += 1
    return res


def gather_data(csv_file):
    '''function to get data'''
    res = {}
    with open(csv_file, 'r', encoding='latin-1') as file:
        reader = csv_module.DictReader(file)
        for row in reader:
            year = row['DATE_OF_REGISTRATION']
            year = year[-2:]  # last 2 str for year format is in DD-MM-YY
            #  to check if year is in the right format
            try:
                int_year = int(year)
            except ValueError:
                continue
            # print(type(int_year))
            business = row['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']
            if 11 <= int_year < 21:
                if business in res:
                    res[business] += 1
                else:
                    res[business] = 1
        sorted_res = sorted(res.items(), key=lambda x: x[1], reverse=True)
        top_5 = [item[0] for item in sorted_res[:5]]
        return top_5


def last_10_year_business_activity(top5_activity_data, csv_file):
    '''returns a nested dict of { year -> { 'business' -> register_count } }'''
    res = {}
    with open(csv_file, 'r', encoding='latin-1') as file:
        reader = csv_module.DictReader(file)
        for row in reader:
            year = row['DATE_OF_REGISTRATION']
            year = year[-2:]  # last 2 str for year format is in DD-MM-YY
            #  to check if year is in the right format
            try:
                int_year = int(year)
            except ValueError:
                continue

            business = row['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']
            if 11 <= int_year < 21 and business in top5_activity_data:
                if int_year in res:
                    if business in res[int_year]:
                        res[int_year][business] += 1
                    else:
                        res[int_year][business] = 1
                else:
                    res[int_year] = {}
                    res[int_year][business] = 1

    return res


def group_plot_business_activity(main_data, top5_activity_data):
    '''plot the group graph'''
    # will store the count of business activity in an ordered manner
    group_2d_list = [[0] * 10 for _ in range(5)]

    # it will tell which year belong to which index 0 will be for 2011
    year_index_dict = last_10_year_dict()

    # it will tell which business belong to which index 0 will be for others
    business_index_dict = list_index_dict(top5_activity_data)

    for year in main_data:
        year_index = year_index_dict[year]
        for business in main_data[year]:
            business_index = business_index_dict[business]
            register_count = main_data[year][business]
            group_2d_list[business_index][year_index] = register_count

    # group plot
    year_list = [2000+i for i in year_index_dict]
    business_list = [i for i in business_index_dict]

    width = 1/6
    x = list(range(10))
    xlist = []
    print(year_list)
    xlist.append(x)
    for i in range(0, 4):
        xlist.append([i+width for i in xlist[i]])
        labels = business_list[i] 
        plt.bar(xlist[i], group_2d_list[i], width, label=labels)
    plt.bar(xlist[4], group_2d_list[4], width, label=business_list[4])

    # Add labels and title
    plt.xlabel('Years')
    plt.ylabel('Register Counts')
    plt.title('Top 5 Prinicipal Business Activity for last 10 year')
    plt.xticks(x, year_list)
    plt.legend()
    plt.show()


def execute():
    '''gather data and plot'''
    csv_file = './Maharashtra.csv'
    top5_activity_data = gather_data(csv_file)
    main_data = last_10_year_business_activity(top5_activity_data, csv_file)
    group_plot_business_activity(main_data, top5_activity_data)


if __name__ == '__main__':
    execute()
# End-of-file (EOF)
