'''To_read_csv_file'''
import csv as csv_module
import matplotlib.pyplot as plt


def gather_data(csv_file):
    '''function to get data'''
    res = []
    with open(csv_file, 'r', encoding='latin-1') as file:
        reader = csv_module.DictReader(file)
        for row in reader:
            m_cap = float(row['AUTHORIZED_CAP'])
            m_cap = int(m_cap)
            res.append(m_cap)
    return res


def histplot(data):
    '''plot a histogram with given data'''
    bins = [0, 1e5, 1e6, 1e7, 1e8, max(data)]
    plt.hist(data, bins=bins, color='skyblue', edgecolor='black')
    plt.xticks(rotation=90)
    plt.xscale('log')
    plt.xlabel('AUTHORIZED_CAP')
    plt.ylabel('Frequency')
    plt.title('Histogram of Authorized Cap')
    plt.show()


def execute():
    '''gather data and plot'''
    csv_file = './Maharashtra.csv'
    m_cap_data = gather_data(csv_file)
    histplot(m_cap_data)


if __name__ == '__main__':
    execute()
# End-of-file (EOF)
